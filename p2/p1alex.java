//import java.io.*; 
//import java.lang.*; 
//import java.util.*;
//--------------------------------------------------------------------------------------------
//Este programa evalua si es correcto la estructura para numeros de tipo 123, 123, 23, enteros
//ademas que verifica los resultados 
//--------------------------------------------------------------------------------------------
class p1aclass
{
 	
 //Variables
  
 static char[] palabra; //Asigna el valor de entrada
 static int i=-1;
 static char c;		//Asigna el caracter uno por uno
 static String msger;   //Lansa mensaje de problema
 static int num;	
 static String sym;
 static String funlec; 

 //Mehtods Importante
 
 public static char get()
 {
   i++;
   System.out.println(palabra[i]);
   return palabra[i];
 }
 	
 public static char putback()
 {
   i--;
   return palabra[i];
 }
 	
 public static void GetCh()
 {
   c = get();  //System.out.println(String.valueOf(c));  //Se asigno para ver variable
 }
 
 public static void GetSym()    //  obtener un nuevo caracter
 {	
     do{
         c= get();
       } while (c==' ');
/*	//CUANDO SE ANEXAN LETRAS SE EMPLEA
     if(Character.isLetter(c))
     {
	System.out.println("Pal:"+c);
	funlec =" ";  //*********falta
	do{
	    funlec += Character.toString(c);
	    c = get();
	}while(Character.isLetter(c));
	c = putback();
	sym="#Fun";
     }
     else   */   //USAR PARA ASIGNACION DE NUMEROS COMPLETOS          
     if(Character.isDigit(c))   //Define si es un digito entre 0 y 9
     {
         num= 0;
         do {
              num= 10*num + c-'0';
              c= get();
            }while(Character.isDigit(c)); //c++ -> while (isdigit(c)); 
         
  	c= putback(); 
 	sym="#NUM";
 	System.out.println("El numero es:"+num+"El simbolo:"+sym);
    }
    else
         switch (c)
	{
          case '+':
          case '-':
          case '*':
          case '/':
          case '(':
          case ')':
          case ';':
                  
              sym=String.valueOf(c);
              System.out.println("El simbolo es:"+sym);  
		 break;
          default:
              sym="#BAD";
              //break;
         }
 }

 public static boolean Is(char q)                 //q es una variable de tipo char
 {
    return sym.compareTo(String.valueOf(q))==0;  //La comparacion se hace entre strings sym es string por default
    //return strcmp(sym, compara)==0;            //Pero q no lo es por tanto se pasa a string con String.valueOf(char c);
 }
	
 public static void Error(String msm)
 {
    msger =msm; 
 }
	
	
 public static void D()
 {
  if('0'<=c && c<='9')
  {
    GetSym();	//GetCh();
  }
   else
     Error("Se espera digito");	
 }

 public static void F()
 {
   if('0'<=c && c<='9')
	D();
   else if(Is('('))
	{
	   GetSym();	//GetCh();
	   E();
	   if(Is(')'))
	     GetSym();	//GetCh();
	   else
	     Error("Falta ')'");
	}
    else if(Is('-'))
	{
	   GetSym();	//GetCh();
	   F();
	}
    else if(Is('+'))
	{
	    GetSym();	//GetCh();
	    F();
	}
    else
	Error("Se espera derivado de F");
 } 

 public static void T() 
 {
  F();
  while(Is('*')|| Is('/'))
      {
	 if(Is('*'))
	    {
	      GetSym();	//GetCh();	
	      F();
	    }
	 else if(Is('/'))
	   {
	      GetSym();	//GetCh();
	      F();
	   }
      }
 }

 public static void E() 
 {
   T();
   while(Is('+') || Is('-'))
	{
	  if(Is('+'))
	       {
		  GetSym(); 	// GetCh();
		  T();
	        }
	  else if(Is('-'))
	        {	
		  GetSym();	//GetCh();
		  T();			
		}
	}
 }

 public static String operando(String cadena)
 {
    palabra = cadena.toCharArray();
    GetSym();
    E();
     if(Is(';'))
        {
	   return "La expresion esta bien";
	}
     else
         {
          return msger;
         }	
 }


}
