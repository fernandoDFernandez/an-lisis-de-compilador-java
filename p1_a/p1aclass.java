//Este programa es llamado por metodo principal manda una palabra a esta clase
//para iniciar a hacer el analisis lexico y usar la gramatica, el cambio es la incorporacione
//de metodos para poder acceder a las palabras.

class p1aclass{
	//Variables

	static char[] palabra;  //Esta palabra llega desde el exterior para ser tratada por la sintaxis
	static int i=-1;	//Definida para leer que al llamar la funcion get al principio se le asigne palabra[0]
	static char c;          //Se asigna a c=palabra[i]
	static String msger;    //Se le asigna mensajes de error en caso de encontrarlos
	//Mehtods Importante

	public static char get() {    //Esta funcion regresa el caracter siguiente de la linea de comandos 
	i++;			      //Define el incremento i=i+1
	return palabra[i];	      //Retorna la palabra siguiente
	}
	
	public static char putback(){ //Esta funcion regresa el caracter anterior al que se mostro
	i--;		   	      //Decrementa el vector "cadena" y regresa un caracer anterior de palabra
	return palabra[i];
	}
	
	public static void GetCh(){   //Asigna la siguiente palabra 
		c = get();
	}
	
	public static void Error(String msm){   //Define errores que se pueden producir
	msger =msm; 
	}
	
	
	public static void D(){		//FORMA PARTE DEL ANALIZADOR LEXICO
	  if('0'<=c && c<='9')		
		GetCh();
	  else
		Error("Se espera digito");
	
	}

	public static void F() {
	   if('0'<=c && c<='9')
		D();
	   else if(c=='(')	//Entra en una asignacion de apertuera de variable ')'
		{
		   GetCh();
		   E();
		   if(c==')')
		     GetCh();
		   else
		     Error("Falta ')'");
		}
	    else if(c=='-')
		{
		   GetCh();
		   F();
		}
	    else if(c=='+')
		{
		    GetCh();
		    F();
		}
	    else
		Error("Se espera derivado de F");

	}

	public static void T() {
	  F();
	  while(c=='*'|| c=='/')
	      {
		 if(c=='*')
		    {
		      GetCh();	
		      F();
		    }
		 else if(c=='/')
		   {
		      GetCh();
		      F();
		   }
	      }

	}

	public static void E() {
	   T();
	   while(c == '+' || c =='-')
		{
		  if(c=='+')
		       {
			  GetCh();
			  T();
		        }
		  else if(c=='-')
		        {	
			  GetCh();
			  T();			
			}
		}
	}

	public static String operando(String cadena){
	palabra = cadena.toCharArray();
	GetCh();
	E();
	     if(c==';')
                {
		   return "La expresion esta bien";
		}
	     else{
		   return msger;
                 }	
	
	}


}
