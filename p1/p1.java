//blic static void F() throws IOException{
           if('0'<=c && c<='9')
                D();
Librerias Necesarias
/*
Este es un programa que opera con digitos entre 0 y 1 con las operaciones de 
suma, resta, multiplicacion, divicion. ademas de verificar que este escrito correctamente
las estrctucturas para operaciones en caso de no ser asi manda un mensaje de error

Este codigo esta escrito usando la siguiente gramatica
     E -> T  { +T | -T }
     T -> F  { *F | /F }
     F -> D | (E) | -F | +F
     D -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |

*/
import java.io.*; 
import java.lang.*;
import java.util.*;
class p1
{
	static char c; //Define un caracter que sera asignado palabras para el codigo
 
	//Este metodo asigna caractar por caracter a lo leido en le buffer
	public static void GetCh() throws IOException{ 
	        c =(char)System.in.read();  
		System.out.println(c); //TESTEO PARA SABER QUE OCURRE EN EL CODIGO
	}

	//Los sigueintes metodos conforman la Gramaticas
	//-----------------------------------------------
        // D -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |	
	public static void D() throws IOException{ //el throws IOException evita que 
	  if('0'<=c && c<='9')		 	   //se generen excepciones opr el GeCh();
		GetCh();		           //Este control forma parte de la POO
	  else
		Error("Se espera digito");
	}

	//----------------------------------------------
	//EL metodo mas importante 
	//F -> D | (E) | -F | +F
	public static void F() throws IOException{
	   if('0'<=c && c<='9')
		D();
	   else if(c=='(')
		{
		   GetCh();
		   E();
		   if(c==')')
		     GetCh();
		   else
		     Error("Falta ')'");
		}
	    else if(c=='-')
		{
		   GetCh();
		   F();
		}
	    else if(c=='+')
		{
		    GetCh();
		    F();
		}
	    else
		Error("Se espera derivado de F");
	}

	//-----------------------------------------
	//El metodo verifica que si es un producto o una division 
	//     T -> F | { *F | /F }
	public static void T() throws IOException{ //
	  F();
	  while(c=='*'|| c=='/')
	      {
		 if(c=='*')
		    {
		      GetCh();	
		      F();
		    }
		 else if(c=='/')
		   {
		      GetCh();
		      F();
		   }
	      }
	}

	//---------------------------------------
    	// E -> T | { +T | -T }
	public static void E() throws IOException{
	   T();
	   while(c == '+' || c =='-')
		{
		  if(c=='+')
		       {
			  GetCh();
			  T();
		        }
		  else if(c=='-')
		        {	
			  GetCh();
			  T();			
			}
		}
	}
	 
	public static void Error(String s){
	   System.out.println("Error - "+s);
	}	

	public static void main(String[] args)throws IOException{
	     GetCh();     //Se asigna previamente el valor correspondiente
	     E();         //Entra al metodo 
	     if(c==';')
                {
		   System.out.println("La expresion esta bien");
		}
	     else{
		   System.out.println("La expresion no esta bien");
                 }
	}

}
