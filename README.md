# Análisis de compilador JAVA para expresiones matemáticas



El presente proyecto incluye partes para el desarrollo de una aplicación para resolver el problema de valores iniciales usando JAVA y PostScript.

p1    Primer programa usa la gramática clasica
      además de hacer una lectura empleando el 
      bufferr y System.in.read() 

p1a   Programa análogo al primero, pero usa una clase
      con la finalidad de implementar metodos para 
      acceder a cada uno de los caracteres, se usan
      los métodos get() putback y Getch()

p2    Este programa evalua si es correcto la estructura 
      para números de tipo 123, 312, 12, además de 
      verificar los resultados

p3    Este programa tiene la finalidad de calcular el valor
      de números además de verificar si la gramática es 
      correcta evaluando un método de tipo calculadora 
     
p4    Este programa permite evaluar una expresión de cualquier
      tipo por ejemplo: x*sin(x)

p5    Este programa puede evaluar usando árboles binarios  

p6    Realiza el mismo trabajo de p5, pero se incorpora un módulo para ver los árboles binarios usando el lenguaje Graphviz

===========================================================================================

Dentro de los códigos se muestran los módulos en Java que forman parte de la aplicación que resuelve 
el problema de valores iniciales. Las clases que incluye son:

Clase principal.java: Integra todas las clases y es donde se pide información al usuario para procesarla posteriormente.

Clase Methods.java: Módulo que incluye métodos numéricos que permiten resolver el PVI.

Clase graficador.java Módulo que incluye los métodos numéricos que permite realizar gráficos usando PostScript para realizar pruebas en la computadora.


==============================================================================================================================
    
    
De forma separada, ecCuad es el programa usando para resolver la ecuación cuadrática:

Incluye el código fuente en Java que permite generar el proyecto de Ecuación cuadrática, cuya finalidad es dar un ejemplo de cómo realizar una aplicación fácil 
en Android Studio.