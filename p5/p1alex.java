public class p1alex{ 
 //Declaracion de nuevas clases -----------------------------------


    public static final int symX=100;
    public static final int symCte=101;
    public static final int symAdd=102;
    public static final int symSub=103;
    public static final int symMul=104;
    public static final int symDiv=105;
    public static final int symSin=106;
    public static final int symCos=107;
    public static final int symSqrt=108;
    public static final int symAtan=109;
    public static final int symLog=110;
    public static final int symExp=111;
    public static final int symChs=112;     

    public static class Node
    {
	public Node lft,rgt;
  	public int type;
	public double ptr;

        public int Type(){
	System.out.println("Se accedio al Type y es:"+type);
	return type;
        }  

        public double Eval(){
	double salida=0.0;
	 switch(Type())
	 {
	    case symX:	        salida= X; break;
	    case symCte:	salida= ptr; break;
	    case symAdd:	salida= lft.Eval() + rgt.Eval(); break;
	    case symSub:	salida= lft.Eval() - rgt.Eval(); break;
	    case symMul:	salida= lft.Eval() * rgt.Eval(); break;
	    case symDiv:	salida= lft.Eval() / rgt.Eval(); break;
	    case symSin:	salida= Math.sin(rgt.Eval());    break;
	    case symCos:	salida= Math.cos(rgt.Eval());    break;
	    case symExp:	salida= Math.exp(rgt.Eval());    break;
	    case symLog:	salida= Math.log(rgt.Eval());	 break;
	    case symAtan:	salida= Math.atan(rgt.Eval());	 break;
	    case symChs:	salida= -1*rgt.Eval();           break;
	}
	return salida;
       }

       public Node(Node l, Node r, int t,double p){
	lft = l;
	rgt = r;
	type = t;
	ptr = p;
       }

       public Node(Node l, Node r, int t){
	lft = l;
	rgt = r;
	type = t;
	}

       public Node(){}
     }

  //----------------------------------------------------------------	
 //Variables
  
 static char[] palabra; 
 static int i=-1;
 static char c;
 static String msger;
 static double num;	//Se redefine type de variable por double
 static String sym;
 static String name;    //Nueva cadena para name
 static String funlec; 
 static double X;       //Nueva variable asignada

 //Mehtods Importante


 public static void m(String s){ System.out.println(s);  } //Mensajear Errores
 public static char get()      { i++; System.out.println(palabra[i]); return palabra[i]; }
 public static char putback()  { i--; System.out.println(palabra[i]); return palabra[i]; }
 public static void GetCh()    { c = get(); System.out.println(String.valueOf(c)); } //Se asigno para ver variable
 
 
  //GENERADOR DE SIMBOLOS
  public static void GetSym()   
 {	
     do{
         c= get();
       } while (c==' ');

	//LETRAS
     if(Character.isLetter(c))
     {
	System.out.println("Pal:"+c);
	name =String.valueOf(c);  //******|***falta
        //System.out.println("Funcion leida es:"+funlec);
	c=get();
        System.out.println("La c vale:"+c);
	if(Character.isLetter(c)){
	   do{
	      name += String.valueOf(c);
	      c = get();
	     }while(Character.isLetter(c));
	}
	c = putback();
	sym="#FUN";
     }
     else      //NUMEROS            
     if(Character.isDigit(c))  
     {
///*
         num= 0;
         do {
              num= 10*num + c-'0';
              c= get();
            }while(Character.isDigit(c));  
         
  	c= putback(); 
//*/	
	//	c=putback();
	 //num=Character.getNumericValue(c); //FUNCIONA COMO DIGITO PERO NO COMO NUMERO  
	//*System.out.println("Posible error:"+c+"y num:"+num);
	sym="#NUM";
 	System.out.println("El numero es:"+num+"El simbolo:"+sym); 
	//Manda un TOKEN de tipo #Num
    }
    else
         switch (c)
	{
          case '+':
          case '-':
          case '*':
          case '/':
          case '(':
          case ')':
          case ';':
                  
              sym=String.valueOf(c);
              System.out.println("El simbolo es:"+sym);  
	      //Manda TOKEN DE TIPO SYMBOLO
		 break;
          default:
              sym="#BAD";
              //break;
         }
 }

 public static boolean Is(String q)                 //q es una variable de tipo char
 {
    System.out.println("Entraste a Is para comparar sym:"+sym+"consta q:"+q+"cyo resultado es"+sym.compareTo(q));
    return sym.compareTo(q)==0;  //La comparacion se hace entre strings sym es string por default
    //return strcmp(sym, compara)==0; //Pero q no lo es por tanto se pasa a string con String.valueOf(char c);
 }
	
 public static void Error(String msm)
 {
    msger =msm; 
 }
	

 public static Node F()
 {
   m("Entraste en F");
   Node f=new Node();
   if(Is("#NUM"))
	{
	
	f=new Node(null,null,symCte,num);
	System.out.println("Numero asignado**:"+num);
	GetSym();
	}
   else 
   if(Is("("))
	{
	   GetSym();	
	   f=E();
	   if(Is(")"))
	     GetSym();
	   else
	     Error("Falta ')'");
	}
    else 
    if(Is("-"))
	{
	   GetSym();
	   f=new Node(null,F(),symChs);
	}
    else 
    if(Is("+"))
	{
	    GetSym();	
	    f=F();
	}
    else 
    if(Is("#FUN"))
    {
	 m("Se accedio a #fun por tanto se le algo");
         String n;
	 n=name;
	   if("x".compareTo(name)==0){ 
		 m("Se crea un nodo de tipo *x*");
	 	 f=new Node(null, null, symX,X);
	 	 GetSym();
	       }
	  else
	  {
 	     GetSym();
	     //**cerr << "name=" << n << endl;
             if(Is("("))
	     {
	  	f=F();
	  	if("sin".compareTo(n)==0)
	  	  f=new Node(null,f,symSin);
	  	else 
		if("cos".compareTo(n)==0)
	  	  f=new Node(null,f,symCos);
	  	else 
		if("sqrt".compareTo(n)==0)
	  	  f=new Node(null,f,symSqrt);
	  	else 
		if("exp".compareTo(n)==0)
	  	  f=new Node(null,f,symExp);
	  	else 
		if("log".compareTo(n)==0)
	  	  f=new Node(null,f,symLog);
	  	else
		if("atan".compareTo(n)==0)
	  	  f=new Node(null,f,symAtan);
	  	else
	  	  Error("Unknown function");
	     }
	     else
		Error("Missing '(')");
	  }
     }
     else{Error("Se espera derivado de F");}

    return f;
 } 

 public static Node T() 
 {
  m("Entraste en T");
  Node t = F();
  while(Is("*")|| Is("/"))
      {
	 if(Is("*"))
	    {
	      GetSym();	
	      t=new Node(t,F(),symMul);
	    }
	 else
	 if(Is("/"))
	   {
	      GetSym();	
	      t=new Node(t,F(),symDiv);
	   }
      }
	return t;
 }

 public static Node E() 
 {
   m("Entraste en E");
   Node e = T();
   while(Is("+") || Is("-"))
	{
	  if(Is("+"))
	       {
		  GetSym(); 	
		  e=new Node(e,T(),symAdd);
	        }
	  else 
	  if(Is("-"))
	        {	
		  GetSym();	
		  e=new Node(e,T(),symSub);			
		}
	}
	return e;
 }

 public static String operando(String cadena)
 {
    palabra = cadena.toCharArray();
    double a=0.0;
    double b=1.0;
    int n=32;
    int j=0;
    GetSym();
    System.out.println("Aqui entra el ciclo");
    Node raiz;
    raiz=E();
     if(Is(";")){
	double h = a+(b-a)/n;
	System.out.println("Casi entra al ciclo");
	for(j=0;j<=n;j++){
		X = a+j*h;
		System.out.println("Se intento evaluar raiz.Eval() ");
		System.out.println("("+X+","+raiz.Eval()+")");
	}      
      //return "La expresion esta bien y su valor es:"+String.valueOf(e.Eval());
      return "SUCCESS";   	
      }
     else{
      return msger;
         }	
 }


}
