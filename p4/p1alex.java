class p1aclass
{
 	
 //Variables
  
 static char[] palabra; 
 static int i=-1;
 static char c;
 static String msger;
 static double num;	//Se redefine type de variable por double
 static String sym;
 static String name;    //Nueva cadena para name
 static String funlec; 
 static double X;       //Nueva variable asignada

 //Metodos Importantes


 public static void m(String s){ System.out.println(s);  } //Mensajear Errores
 public static char get()      { i++; System.out.println(palabra[i]); return palabra[i]; }
 public static char putback()  { i--; System.out.println(palabra[i]); return palabra[i]; }
 public static void GetCh()    { c = get(); System.out.println(String.valueOf(c)); } //Se asigno para ver variable
 
 
  //GENERADOR DE SIMBOLOS
  public static void GetSym()   
 {	
     do{
         c= get();
       } while (c==' ');

	//LETRAS
     if(Character.isLetter(c))
     {
	System.out.println("Pal:"+c);
	name =String.valueOf(c);  //******|***falta
        System.out.println("Funcion leida es:"+funlec);
	c=get();
        System.out.println("La c vale:"+c);
	if(Character.isLetter(c)){
	   do{
	      name += String.valueOf(c);
	      c = get();
	     }while(Character.isLetter(c));
	}
	c = putback();
	sym="#FUN";
     }
     else      //NUMEROS            
     if(Character.isDigit(c))  
     {
///*
         num= 0;
         do {
              num= 10*num + c-'0';
              c= get();
            }while(Character.isDigit(c));  
         
  	c= putback(); 
//*/	
	//	c=putback();
	 //num=Character.getNumericValue(c); //FUNCIONA COMO DIGITO PERO NO COMO NUMERO  
	//*System.out.println("Posible error:"+c+"y num:"+num);
	sym="#NUM";
 	System.out.println("El numero es:"+num+"El simbolo:"+sym); 
	//Manda un TOKEN de tipo #Num
    }
    else
         switch (c)
	{
          case '+':
          case '-':
          case '*':
          case '/':
          case '(':
          case ')':
          case ';':
                  
              sym=String.valueOf(c);
              System.out.println("El simbolo es:"+sym);  
	      //Manda TOKEN DE TIPO SYMBOLO
		 break;
          default:
              sym="#BAD";
              //break;
         }
 }

 public static boolean Is(String q)                 //q es una variable de tipo char
 {
    System.out.println("Entraste a Is para comparar sym:"+sym+"consta q:"+q+"cyo resultado es"+sym.compareTo(q));
    return sym.compareTo(q)==0;  //La comparacion se hace entre strings sym es string por default
    //return strcmp(sym, compara)==0; //Pero q no lo es por tanto se pasa a string con String.valueOf(char c);
 }
	
 public static void Error(String msm)
 {
    msger =msm; 
 }
	

 public static double F()
 {
   m("Entraste en F");
	 double f=0.0;
   if(Is("#NUM"))
	{
	f=num;
	System.out.println("Numero asignado**:"+num);
	GetSym();
	}
   else 
   if(Is("("))
	{
	   GetSym();	
	   f=E();
	   if(Is(")"))
	     GetSym();
	   else
	     Error("Falta ')'");
	}
    else 
    if(Is("-"))
	{
	   GetSym();
	   f-=F();
	}
    else 
    if(Is("+"))
	{
	    GetSym();	
	    f+=F();
	}
    else 
    if(Is("#FUN"))
    {
         String n;
	 n=name;
	   if("x".compareTo(name)==0){ 
	 	 f=X;
	 	 GetSym();
	       }
	  else
	  {
 	     GetSym();
	     //**cerr << "name=" << n << endl;
             if(Is("("))
	     {
	  	f=F();
	  	if("sin".compareTo(n)==0)
	  	  f=Math.sin(f);
	  	else 
		if("cos".compareTo(n)==0)
	  	  f=Math.cos(f);
	  	else 
		if("sqrt".compareTo(n)==0)
	  	  f=Math.sqrt(f);
	  	else 
		if("exp".compareTo(n)==0)
	  	  f=Math.exp(f);
	  	else 
		if("log".compareTo(n)==0)
	  	  f=Math.log(f);
	  	else
		if("atan".compareTo(n)==0)
	  	  f=Math.atan(f);
	  	else
	  	  Error("Unknown function");
	     }
	     else
		Error("Missing '(')");
	  }
     }
     else
       Error("Se espera derivado de F");
    return f;
 } 

 public static double T() 
 {
  m("Entraste en T");
  double t = F();
  while(Is("*")|| Is("/"))
      {
	 if(Is("*"))
	    {
	      GetSym();	
	      t*=F();
	    }
	 else
	 if(Is("/"))
	   {
	      GetSym();	
	      t/=F();
	   }
      }
	return t;
 }

 public static double E() 
 {
   m("Entraste en E");
   double e = T();
   while(Is("+") || Is("-"))
	{
	  if(Is("+"))
	       {
		  GetSym(); 	
		  e+=T();
	        }
	  else 
	  if(Is("-"))
	        {	
		  GetSym();	
		  e-=T();			
		}
	}
	return e;
 }

 public static String operando(String cadena)
 {
    palabra = cadena.toCharArray();
    X=1.0;
    
    GetSym();
    System.out.println("Aqui entra el ciclo");
    double e=E();
     if(Is(";")){
      return "La expresion esta bien y su valor es:"+String.valueOf(e);
         	}
     else{
      return msger;
         }	
 }


}
