public class p1alex{ 
    private double valor; 
    private p1alex p1;

    public void Funciones(p1alex p1)
    {
	this.p1 = p1;
    } 

    public double P1(double var_x, double var_y){
        double valor = p1.funcion(var_x,var_y);
	return valor; 
    }

    public enum simbolos{
	symX, symCte, symAdd, symSub, symMul, symDiv, symSin,
	symCos, symSqrt, symAtan, symLog, symExp, symChs, symY, symW,
	symp1
    };
    
    public class Node{
      private Node lft,rgt;
      private simbolos type;
      private double ptr;
      
      public simbolos Type(){
        return type;
      }
    
      public double Eval(){
        double salida=0.0;
        switch(Type())
        {
          case symW:          salida = W;                       break;
          case symX:          salida = X;                       break;
          case symY:          salida = Y;                       break;
          case symCte:        salida = ptr;                     break;
          case symAdd:        salida = lft.Eval() + rgt.Eval(); break;
          case symSub:        salida = lft.Eval() - rgt.Eval(); break;
          case symMul:        salida = lft.Eval() * rgt.Eval(); break;
          case symDiv:        salida = lft.Eval() / rgt.Eval(); break;
          case symSin:        salida = Math.sin(rgt.Eval());    break;
          case symCos:        salida = Math.cos(rgt.Eval());    break;
          case symExp:        salida = Math.exp(rgt.Eval());    break;
          case symLog:        salida = Math.log(rgt.Eval());    break;
          case symAtan:       salida = Math.atan(rgt.Eval());   break;
          case symChs:        salida = -1*rgt.Eval();           break;
	  case symp1:	      salida = P1(rgt.Eval(),0);        break;
        }
        return salida;
      }
          
       
      public Node(Node hijo_izquierdo, Node hijo_derecho, simbolos symbol,double constante)
      {
        lft = hijo_izquierdo;
        rgt = hijo_derecho;
        type = symbol; 
        ptr = constante;
      }
      
      public Node(Node hijo_izquierdo, Node hijo_derecho, simbolos symbol)
      {
        lft = hijo_izquierdo;
        rgt = hijo_derecho;
        type = symbol;  
      }
       
      public Node(){}

      public void Modi(Node hijo_izquierdo, Node hijo_derecho, simbolos symbol, double constante)
      {
        lft  = hijo_izquierdo;
        rgt  = hijo_derecho;
        type = symbol;
        ptr  = constante;
      }
     
      public void Modi(Node hijo_izquierdo, Node hijo_derecho, simbolos symbol)
      {
        lft=hijo_izquierdo;
        rgt=hijo_derecho;
        type=symbol;
      }
    }

 //Variables
 private char[] palabra; 
 private int i=-1;
 private char c;
 private String msger;
 private double num;	//Se redefine type de variable por double
 private double num2;
 private int contador;
 private String sym;
 private String name;    //Nueva cadena para name
 private String funlec; 
 private double X;       //Nueva variable asignada
 private double Y;
 private double W;

 //Metodos Importantes
 public void m(String s){ System.out.println(s);  } 
 public char get()      { i++; return palabra[i]; }
 public char putback()  { i--; return palabra[i]; }

  //GENERADOR DE SIMBOLOS
  public void GetSym()   
  {	
    do
    {
      c= get();
    } while (c==' ');
    if(Character.isLetter(c))
    {
      
      name =String.valueOf(c);  
      c=get();
      while(Character.isLetter(c)){
        name += String.valueOf(c);
	c = get();
      }
      c = putback();
      sym="#FUN";
    }
    //Entra al estado q_0
    else if(Character.isDigit(c)||c=='.')  
    {
      num= 0;
      num2=0;
      contador=0;

      if(Character.isDigit(c)){
      //Entra al estado q_1
          do 
	  {
	      num= 10*num + c-'0';
	      c= get();
	  }while(Character.isDigit(c));
      	  sym="#NUM";
	     
	   if(c=='.'){
	       //Entra al estado q_3
	       c=get();
	       if(Character.isDigit(c)){
		   //Entra al estado q_5
                   do
               	   {
               	       num2= 10*num2 + c-'0';
               	       contador++;
               	       c= get();
               	   }while(Character.isDigit(c));
              	   num=num+num2*Math.pow(10,-1*contador);
               	   sym="#NUM";
               	   if(c=='.'){
		       //Entra al estado q_6
               	       sym = "BAD";
		   }

       	       }else if(c=='.'){
                   //Entra al estado q_6
       	           sym ="#BAD";
               }else{
       	           sym ="#BAD";
	       }
	   }

      }else{
	//Entra al estado q_2
        c=get();
	if(Character.isDigit(c)){
	    //Entra al estado q_4
	    do 
	    {
	        num2= 10*num2 + c-'0';
		contador++;
		c= get();
	    }while(Character.isDigit(c));
	    num=num+num2*Math.pow(10,-1*contador);  
            sym="#NUM";

	    if(c=='.'){
	        sym = "BAD";
	    }
	
	}else if(c=='.'){
		//Entra al estado q_6
		sym ="#BAD";
	}else{
		sym ="#BAD";
	}
     }
       
     c= putback(); 
    } 
    else 
    {
      switch (c)
      {
        case '+':
        case '-':
        case '*':
        case '/':
        case '(':
        case ')':
        case ';':          
        sym=String.valueOf(c); break; 
        default:
        sym="#BAD"; break;
      }
    }
  }

 public boolean Is(String q)               
 {
    return sym.compareTo(q)==0;  
 }

 public boolean Is2(String q1)
 {
    return name.compareTo(q1)==0;
 }
	
 public void Error(String msm)
 {
   if(msger==null)
   {
   	msger="ERROR:"+msm;
   }
   else
   {
    	msger+="/"+msm; 
   }
 }
	

 public Node F()
 {
   Node f=new Node();
   if(Is("#NUM"))
   {
        f.Modi(null, null, simbolos.symCte, num);
        GetSym();
   }
   else if(Is("("))
   {
     GetSym();	
     f=E();
     if(Is(")"))
     {
       GetSym();
     }
     else
     {
       Error("Falta ')'");
     }
   }
   else if(Is("-"))
   {
         GetSym();
         f.Modi(null, F(), simbolos.symChs);
   }
   else if(Is("+"))
   {
   	GetSym();	
   	f=F();
   }
   else if(Is("#FUN"))
   {
    	String n;
   	 n=name; 
	
     if("x".compareTo(name)==0 ||"y".compareTo(name)==0||"w".compareTo(name)==0)
     {
       if("x".compareTo(name)==0)
       {
                f.Modi(null, null, simbolos.symX, X);
       }  
       if("y".compareTo(name)==0)
       { 
               f.Modi(null, null, simbolos.symY, Y); 
       }   
       if("w".compareTo(name)==0)
       { 
               f.Modi(null, null, simbolos.symW, W);
       }  
       GetSym();
     }
     else if(Is2("sin")||Is2("cos")||Is2("sqrt")||Is2("exp")||Is2("log")||Is2("atan")||Is2("h"))
     {
       GetSym();
       if(Is("("))
       {
           if("sin".compareTo(n)==0)
           {
                   f.Modi(null,F(),simbolos.symSin);
           }
	   else if("cos".compareTo(n)==0)
           {
	           f.Modi(null,F(), simbolos.symCos);
	   }
	   else if("sqrt".compareTo(n)==0)
           {    
                   f.Modi(null, F(), simbolos.symSqrt);
	   }	
           else if("exp".compareTo(n)==0)
           {
                   f.Modi(null, F(), simbolos.symExp);
	   }   	
           else if("log".compareTo(n)==0)
           {
                   f.Modi(null, F(), simbolos.symLog);
	   }
           else if("h".compareTo(n)==0)
	   {
		   f.Modi(null, F(), simbolos.symp1);
	   }		
           else if("atan".compareTo(n)==0)
           {
                   f.Modi(null, F(), simbolos.symAtan);
           }
           else //ESTA DE MAS EL ELSE*******************
           {
	      	   Error("Unknown function");
           }
       } 
       else
       {
          Error("Missing '(')");           
       }
     }
     else
     {
       Error("Funcion Desconocida"); 
     }
   }
   else if(Is("#BAD"))   //Cuando usar
   {
    Error("Error de anomalia");
   }
   else
   {
     Error("Se espera derivado de F");   //CUANDO HACER QUE APAREZCA
   }
   return f;
 } 

 public Node T() 
 {
  Node t = F();
  while(Is("*")|| Is("/"))
      {
	 if(Is("*"))
	    {
	      GetSym();	
	      t=new Node(t,F(),simbolos.symMul);   //********
	    }
	 else
	 if(Is("/"))
	   {
	      GetSym();	
	      t=new Node(t,F(),simbolos.symDiv);    //*******
	   }
      }
      return t;
 }

 public Node E() 
 {
   Node e = T();
   while(Is("+") || Is("-"))
   {
	  if(Is("+"))
	       {
		  GetSym(); 	
		  e=new Node(e,T(),simbolos.symAdd);   
	        }
	  else 
	  if(Is("-"))
	        {	
		  GetSym();	
		  e=new Node(e,T(),simbolos.symSub);   
		}
   }
	return e;
 }


 public Node raiz;

 public String operando(String cadena)
 {
    palabra = cadena.toCharArray();
    GetSym();
    raiz=E();
    if(Is(";"))
    {   
     if(msger!=null)
     {
       raiz=null;
     }
     if(msger==null)
     {
       msger = "SUCCESS";
     }
    }
    else{
      if(msger==null){ msger = "Error:Funcion o constante  desconocida"; }
    msger = msger+", revisar sintaxis";
    raiz=null;
    }
   return msger;
 }
 
 public double funcion(double valor_de_x){
 	Double notNumber = new Double (Double.NaN);
	Double aux;
	X=valor_de_x;
	double val = raiz.Eval();
	aux=new Double(val);
        if(aux.equals(notNumber)){
        val = 0;
        System.out.println("Error fatal");
        }
	return val; 
 }

 public double funcion(double valor_de_x,double valor_de_y, double valor_de_w){
	X=valor_de_x;
	Y=valor_de_y;
	W=valor_de_w;
	return raiz.Eval(); 
 }

 public double funcion(double valor_de_x, double valor_de_y){
	Double notNumber = new Double (Double.NaN);
	Double aux;
	X=valor_de_x; 	
	Y=valor_de_y;
	double val = raiz.Eval();
	aux= new Double(val);
	if(aux.equals(notNumber)){
	val = 0;
	System.out.println("Error fatal");
	}
	return val;
 }

}
