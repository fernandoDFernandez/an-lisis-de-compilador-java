public class Methods {

    private p1alex diferencial;
    
    public void asignarEcuacionLexica(p1alex entrada){  
      diferencial = entrada;
    }

    public double f(double x,double y){
      return diferencial.funcion(x,y); 
    }

    //FUNCION PARA 3 VARIABLES
    public double f(double x, double y, double w){
	return diferencial.funcion(x,y,w);
    }

    //DOMINIO DE LA FUNCION 
    public double[] ReturnX(double x0, double xu, double y0, int n){
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       x[0] = x0;
          for(int i=0;i<n;i++)
          {
              x[i+1] = x[i]+h;
          }
       return x;
    }

    //METODOS RUNGE KUTA DE UN PASO 

         //METODO DE EULER
    public double[] Eulermethod(double x0, double xu, double y0, int n){
       double[] y = new double[n+1];
       double[] r = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       r[0] = x0;
       y[0] = y0;
       for(int i=0;i<n;i++)
       {
           r[i+1] = r[i]+h;
           y[i+1] = y[i]+h*f(r[i],y[i]);
       }
       return y;
    }

    //METODO DE EULER PARA UN SISTEMA DE ECUACIONES 2X2
    public double[] Eulermethod(double x0, double xu, double y0, double w0, int n)
    {
	double[] w = new double[n+1];   
	double[] y = new double[n+1];
	double[] r = new double[n+1];
	double h=Math.abs(x0-xu)/n;
	r[0]=x0;
	y[0]=y0;
	w[0]=w0;
	for(int i=0;i<n;i++)
	{
	    r[i+1]=r[i]+h;
	    w[i+1]=w[i]+h*f(r[i],y[i],w[i]);
	    y[i+1]=y[i]+h*w[i];
	}
	return y;
    }

    //METODOD DE PUNTO MEDIO
    public double[] RKord2a1(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] r = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       r[0] = x0;
       y[0] = y0;
       double k1 = 0;
       double k2 = 0;
       //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
           r[j+1] = r[j]+h;
       }
       //Define entradas para el eje y
       for(int i=0;i<n;i++)
       {
           k1 = h*f(r[i],y[i]);
           k2 = h*f(r[i]+(h/2),y[i]+(k1/2));
           y[i+1] = y[i]+k2;
       }
       return y; 
    }
    
    //EULER MODIFICADO
    public double[] RKord2a1m2(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] r = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       r[0] = x0;
       y[0] = y0;
       double k1 = 0;
       double k2 = 0;
       //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
           r[j+1] = r[j]+h;
       }
       //Define entradas para el eje y
       for(int i=0;i<n;i++)
       {
           k1 = h*f(r[i],y[i]);
           k2 = h*f(r[i]+h,y[i]+k1);
           y[i+1] = y[i]+0.5*(k1+k2);
       }
       return y; 
    }
    
    //METODO DE HEUN
    public double[] RKord2a3m2(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] r = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       r[0] = x0;
       y[0] = y0;
       double k1 = 0;
       double k2 = 0;
       //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
           r[j+1] = r[j]+h;
       }
       //Define entradas para el eje y
       for(int i=0;i<n;i++)
       {
           k1 = h*f(r[i],y[i]);
           k2 = h*f(r[i]+(2.0/3)*h,y[i]+(2.0/3)*k1);
           y[i+1] = y[i]+(1.0/4)*(k1+3*k2);
       }
       return y;               
    }    
        //Runge-Kuta Orden 4
    public double[] RKord4(double x0, double xu, double y0, int n){
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       x[0] = x0;
       y[0] = y0;
       double k1 = 0;
       double k2 = 0;
       double k3 = 0;
       double k4 = 0;
       //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
           x[j+1] = x[j]+h;
       }
       //Define entradas para el eje y
       for(int i=0;i<n;i++)
       {
           k1 = h*f(x[i],y[i]);
           k2 = h*f(x[i]+(h/2),y[i]+(k1/2));
           k3 = h*f(x[i]+(h/2),y[i]+(k2/2));
           k4 = h*f(x[i]+h,y[i]+k3);
           y[i+1] = y[i]+(1.0/6)*(k1+2*k2+2*k3+k4);
       }
       return y;
    }    

    //METODOS MULTIPASO //////////////
        //METODOS EXPLICITOS
    //Expplicito dos pasos generadondo condiciones iniciales con punto medio
    public double[] Exp2pasos(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       x[0] = x0;
       y[0] = y0;
  
          //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
           x[j+1] = x[j]+h;
       }             
       //Define entradas para el eje y
       y[1] = y[0]+h*f(x[0]+(h/2.0),y[0]+(h/2.0)*f(x[0],y[0]));
       for(int i=1;i<n;i++)
       {
           y[i+1] = y[i]+(h/2.0)*(3*f(x[i],y[i])-f(x[i-1],y[i-1]));
       }
       return y;
               
    }    

    //Explicito tres pasos generado condicones iniciales con punto medio
    public double[] Exp3pasos(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       x[0] = x0;
       y[0] = y0;
       for(int j=0;j<n;j++)
       {
           x[j+1] = x[j]+h;
       }
       //Define entradas para el eje y   
       y[1] = y[0]+h*f(x[0]+(h/2.0),y[0]+(h/2.0)*f(x[0],y[0]));   //Se define previamente
       y[2] = y[1]+h*f(x[1]+(h/2.0),y[1]+(h/2.0)*f(x[1],y[1])); 
       for(int i=2;i<n;i++)
       {
           y[i+1] = y[i]+(h/12.0)*(23*f(x[i],y[i])-16*f(x[i-1],y[i-1])+5*f(x[i-2],y[i-2]));
       }
       return y;
    }
                
    //Explicito cuatro pasos generado condicones iniciales con punto medio 
    public double[] Exp4pasos(double x0, double xu, double y0, int n){
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       x[0] = x0;
       y[0] = y0;

       //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
           x[j+1] = x[j]+h;
       }
       //Define entradas para el eje y   
       y[1] = y[0]+h*f(x[0]+(h/2),y[0]+(h/2)*f(x[0],y[0]));   //Se define previamente
       y[2] = y[1]+h*f(x[1]+(h/2),y[1]+(h/2)*f(x[1],y[1])); 
       y[3] = y[2]+h*f(x[2]+(h/2),y[2]+(h/2)*f(x[2],y[2]));     
       for(int i=3;i<n;i++)
       {
           y[i+1] = y[i]+(h/24)*(55*f(x[i],y[i])-59*f(x[i-1],y[i-1])+37*f(x[i-2],y[i-2])-9*f(x[i-3],y[i-3]));
       }
       return y;
    }
    
        //METODOS IMPLICITOS
    //METODO DE SEGUNDO ORDEN CON PREDICTOR DE TIPO EULER
    public double[] Imp2Orden(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       //int lo = a.length;
       x[0] = x0;
       y[0] = y0;

       //Define las entradas del dominio "X"
       for(int j=0;j<n;j++)
       {
       		x[j+1] = x[j]+h;
       }
       
       //Define entradas para el eje y   
       y[1] = y[0]+h*f(x[0]+(h/2),y[0]+(h/2)*f(x[0],y[0]));   //Se define previamente
     
       for(int i=1;i<n;i++)
       {
       		y[i+1] = y[i]+(h/12)*(5*f(x[i+1],y[i]+h*f(x[i],y[i]))+8*f(x[i],y[i])-f(x[i-1],y[i-1]));
       }
       return y;
    }
    //METODO DE TERCER ORDEN CON PREDICTOR DE TIPO EULER
    public double[] Imp3Orden(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       //int lo = a.length;
       x[0] = x0;
       y[0] = y0;

       //Define las entradas del dominio "X"
       for(int j=0;j<n;j++)
       {
           x[j+1] = x[j]+h;
       }
       //Define entradas para el eje y   
       y[1] = y[0]+h*f(x[0]+(h/2),y[0]+(h/2)*f(x[0],y[0]));   //Se define previamente
       y[2] = y[1]+h*f(x[1]+(h/2),y[1]+(h/2)*f(x[1],y[1]));
       for(int i=2;i<n;i++)
       {
           y[i+1] = y[i]+(h/24)*(9*f(x[i+1],y[i]+h*f(x[i],y[i]))+19*f(x[i],y[i])-5*f(x[i-1],y[i-1])+f(x[i-2],y[i-2]));
       }
       return y;
    }
    //METODO DE TERCER ORDEN CON PREDICTOR DE TIPO EULER
    public double[] Imp4Orden(double x0, double xu, double y0, int n)
    {
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       //int lo = a.length;
       x[0] = x0;
       y[0] = y0;
       //Define las entradas del dominio "X"
       for(int j=0;j<n;j++)
       {
           x[j+1] = x[j]+h;
       }
       //Define entradas para el eje y   
       y[1] = y[0]+h*f(x[0]+(h/2),y[0]+(h/2)*f(x[0],y[0]));   //Se define previamente
       y[2] = y[1]+h*f(x[1]+(h/2),y[1]+(h/2)*f(x[1],y[1]));   //Extrano
       y[3] = y[2]+h*f(x[2]+(h/2),y[2]+(h/2)*f(x[2],y[2]));   //Extraio
       for(int i=3;i<n;i++)
       {
         y[i+1] = y[i]+(h/720)*(251*f(x[i+1],y[i]+h*f(x[i],y[i]))+646*f(x[i],y[i])-264*f(x[i-1],y[i-1])+106*f(x[i-2],y[i-2])-19*f(x[i-2],y[i-2]));
       }
       return y;
    }
 
    //PREDICTOR CORRECTOR
    public double[] Pred_Corre(double x0, double xu, double y0, int n){
       double[] p = new double[n+1];
       double[] y = new double[n+1];
       double[] x = new double[n+1];
       double h = Math.abs(x0-xu)/n;
       x[0] = x0;
       p[0] = y0;
       y[0] = y0;
       //Define las entradas para el eje x
       for(int j=0;j<n;j++)
       {
            x[j+1] = x[j]+h;
       }
       //Define entradas para el eje y   
       p[1] = p[0]+h*f(x[0]+(h/2),p[0]+(h/2)*f(x[0],p[0]));   //Se define usando el metodo
       p[2] = p[1]+h*f(x[1]+(h/2),p[1]+(h/2)*f(x[1],p[1]));   //de PUNTO MEDIO
       p[3] = p[2]+h*f(x[2]+(h/2),p[2]+(h/2)*f(x[2],p[2])); 
       y[1] = p[1];   //Se define previamente
       y[2] = p[2];   //Extrano
       y[3] = p[3];   //Extrano
       for(int i=3;i<n;i++){
           p[i+1] = p[i]+(h/24)*(55*f(x[i],p[i])-59*f(x[i-1],p[i-1])+37*f(x[i-2],p[i-2])-9*f(x[i-3],p[i-3]));
       }
       for(int i=3;i<n;i++){
           y[i+1] = y[i]+(h/720)*(251*f(x[i+1],p[i+1])+646*f(x[i],y[i])-264*f(x[i-1],y[i-1])+106*f(x[i-2],y[i-2])-19*f(x[i-2],y[i-2]));
       }
       return y;
    }
}
