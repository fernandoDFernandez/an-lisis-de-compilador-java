import java.util.*; 
import java.io.*;

class principal{
    public static void main(String[] args){
  	Scanner teclado  = new Scanner(System.in);  
 	p1alex EDO       = new p1alex(); 
	p1alex hdex      = new p1alex();          
        String EDOestado = " ", hdexEstado = " ";                    
	PrintStream Pantalla = System.out;

        Pantalla.println("***   Resolvedor numerico de ecuaciones diferenciales ordinaras   ***");	
	//Pantalla.println(" ");
	
	Pantalla.println("Ingrese el intervalo [x0,xu] primero x0 [ENTER] enseguida xu [ENTER]");
	Pantalla.print(">>");
	double x0 = teclado.nextDouble();
	double xu = teclado.nextDouble();

	Pantalla.println("Ingrese 3 condiciones de frontera 'yo' f(x0)=y0 y el numero de pasos m ");
	Pantalla.print(">>");
	double y0 = teclado.nextDouble();
	double y1 = teclado.nextDouble();
	double y2 = teclado.nextDouble();
        int m     = teclado.nextInt();
	teclado.nextLine();

	Pantalla.println("Introduce Ecuacion Diferencial:");
	Pantalla.print(" ");
	Pantalla.print(">>  df/dx = ");

	String cadena_de_texto_EDO_a_resolver = teclado.nextLine();
        cadena_de_texto_EDO_a_resolver =  cadena_de_texto_EDO_a_resolver+";";
 	Pantalla.println("f ':"+cadena_de_texto_EDO_a_resolver); 
	
	Pantalla.println("Ecuacion Auxiliar 1:");	
	Pantalla.print(">> h(x) = ");
	String AnalizadorLexico = teclado.nextLine()+";";
	Pantalla.println("h(x) ="+AnalizadorLexico); //Se imprime info de la otra funcion
	
        hdexEstado = hdex.operando(AnalizadorLexico);
	Pantalla.println("Estado h(x):"+hdexEstado);
		
	if(hdexEstado =="SUCCESS"){	
	EDO.Funciones(hdex);	
	EDOestado     = EDO.operando(cadena_de_texto_EDO_a_resolver);
        Pantalla.println("EDO:ESTADO :"+EDOestado);
	}else{
		Pantalla.println("Error en h(x)");
	}

        if(EDOestado=="SUCCESS")
        {
          	
          Methods solucion_numerica = new Methods();     
          solucion_numerica.asignarEcuacionLexica(EDO);	 
          
          double[] coordenadas_x; 
          double[] coordenadas_Y_condicion_de_frontera1; 
          double[] coordenadas_Y_condicion_de_frontera2;
          double[] coordenadas_Y_condicion_de_frontera3; 
          double w0 = 0;
          //En caso de no usar variables de entrada=========>>>> double x0 = 0, xu = 2*(3.1416), y0 = 0;
          coordenadas_x = solucion_numerica.ReturnX(x0, xu , y0, m);  //No importa el y0

          //Dado un intervalo (x0,xu) donde f(x0)=y0
          //resulado = metodo.TipoDeAproximacion(x0,xu,y0,n);
          //METODOS DE LA CLASE METHOD DE TIPO PUBLICO Y REGRESAN TIPO DE DATO double
          //              Eulermethod(0,1,1,m);         //Metodo de Euler
          //              RKord2a1(0, 1, 1, m);         //Metodo de Runge Kuta orden 1 alfa 1
          //              RKord2a1m2(0, 1, 1, m);       //Metodo de Runge Kuta orden 1 alfa 1/2
          //              RKord2a3m2(0, 1, 1, m);       //Metodo de Runge orden 1 Kuta alfa 3/4

          coordenadas_Y_condicion_de_frontera1 = solucion_numerica.RKord4(x0, xu, y0, m);
          coordenadas_Y_condicion_de_frontera2 = solucion_numerica.RKord4(x0, xu, y1, m);
          coordenadas_Y_condicion_de_frontera3 = solucion_numerica.RKord4(x0, xu, y2, m);

          // Exp2pasos(0, 1, 1, m);        //Metodo Explicito 2 pasos 
          // Exp3pasos(0, 1, 1, m);        //Metodo Explicito 3 pasos
          // Exp4pasos(0, 1, 1, m);        //Metodo Explicito 4 pasos; Tiene problemas
          // Imp2Orden(0, 1, 1, m);        //Implicito de segundo orden 
          // Imp3Orden(0, 1, 1, m);        //Implicito de tercer  orden 
          // Imp4Orden(0, 1, 1, m);        //Implicito de cuarto  orden; Tiene problemas
          // Pred_Corre(0, 1, 1, m);       //Predictor Corrector ; Tiene Problemas
    
	  //Muestra en pantalla los resultados
           double maximo1=0;
           double maximo2=0;
           double[][] matriz =new double[m+1][4];
           for(int j=0;j<(m+1);j++)
           {
             matriz[j][0]=coordenadas_x[j];
             maximo1=(maximo1>Math.abs(coordenadas_x[j]))? maximo1:Math.abs(coordenadas_x[j]);
             matriz[j][1] = coordenadas_Y_condicion_de_frontera1[j];
             matriz[j][2] = coordenadas_Y_condicion_de_frontera2[j];
             matriz[j][3] = coordenadas_Y_condicion_de_frontera3[j];            
             maximo2 = (maximo2>Math.abs(coordenadas_Y_condicion_de_frontera1[j]))? maximo2:Math.abs(coordenadas_Y_condicion_de_frontera1[j]);
             maximo2 = (maximo2>Math.abs(coordenadas_Y_condicion_de_frontera2[j]))? maximo2:Math.abs(coordenadas_Y_condicion_de_frontera2[j]);
             maximo2 = (maximo2>Math.abs(coordenadas_Y_condicion_de_frontera3[j]))? maximo2:Math.abs(coordenadas_Y_condicion_de_frontera3[j]);
             //Pantalla.println(coordenadas_x[j]+","+coordenadas_Y_condicion_de_frontera1[j]);
           } 
           //Grafica los resultados
           graficador grafico = new graficador();
           grafico.asignarmatriz(matriz,maximo1,maximo2);
           grafico.generador();
	   /*try{
	     String url = "home/fernandod/Desktop/compiladorjavalex/archivo.ps";
	     ProcessBuilder p = new ProcessBuilder();
             p.command("evince archivo.ps",url);
             p.start();
           }catch(IOException e){
              Pantalla.println("Ocurrio error");
              //Logger.getLogger(principal.class.getName()).log(Level.SEVERE,null,e);
           }	   */
       }

   }
}
