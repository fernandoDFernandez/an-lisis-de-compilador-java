import java.io.*;
class graficador{  
  private double[][] matriz;
  double  scx=1;
  double scy=1;
  public void asignarmatriz(double[][] entrada,double maxx,double maxy){
     matriz=entrada;
     scy=8.0/maxy;
     scx=8.0/maxx;
  }
  public void generador(){ 
     try{  //**** Se crea el arreglo
        File archivo = new File("archivo.ps");
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter(archivo)); //Se genera Archivo fuente
	//Define el tipo de lapiz, tamaño de la punta y tamaño del papel a usar.
	bw.write("#!PS\n");   //Se usa en ubuntu 
        //bw.write("%%!PS\n"); //Se usa en windows
        bw.write("8.5 72 mul 2 div 11.0 72 mul 2 div translate\n");
	bw.write("72 2.54 div dup scale\n");
        bw.write("2 setlinecap\n");
        bw.write("0.05 setlinewidth\n" );
	bw.write("-8"+" "+"0"+" moveto\n"); //Define Ejes X vs Y
	bw.write("8"+" "+"0"+" lineto\n");
	bw.write("0"+" "+"8"+" moveto\n");
	bw.write("0"+" "+"-8"+" lineto\n");
	bw.write("0"+" "+"0"+" moveto\n"); //Regresa al origen
	bw.write("stroke\n");
	bw.write("0.11 0.56 1 setrgbcolor\n");
	bw.write("0"+" "+"0"+" moveto\n");
        //bw.write("Escribe lo que quieres en el archivo")
        int valor = matriz.length;
        int contador =0;
        //System.out.println("Longitud de la entrada Matriz="+matriz[0].length);
	bw.write(scx*matriz[0][0]+" "+scy*matriz[0][1]+" moveto\n");
	for (int k=1; k<valor; k++){  
	   bw.write(scx*matriz[k][0]+" "+scy*matriz[k][1]+" lineto\n");
	}     
	if(matriz[0].length>2){
	   for(int nue=2;nue<matriz[0].length;nue++){
	      contador++;
	      bw.write("0"+" "+"0"+" moveto\n");
	      bw.write("stroke\n");
	      if(contador==1) {bw.write("0.85 0.64 0.12 setrgbcolor\n");}
	      if(contador==2) {bw.write("0.6 0.8 0.1960 setrgbcolor\n");}
	      bw.write("0"+" "+"0"+" moveto\n");
	      bw.write(scx*matriz[0][0]+" "+scy*matriz[0][nue]+" moveto\n");
	      for (int k=1; k<valor; k++){  
	         bw.write(scx*matriz[k][0]+" "+scy*matriz[k][nue]+" lineto\n");
	      }
	   }
	}
	bw.write("stroke\n");
        bw.write("showpage\n");
        bw.close();  //Se cierra el archivo
     }
     catch(IOException e){
        System.out.println("Error"+e.toString());
     }
  }
}
